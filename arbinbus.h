#ifndef ARBINBUS_H
#define ARBINBUS_H
#include <nodebt.h>

//DECLARACIONES
template <typename T>
class arbinbus{
    //ENTIDADES PÚBLICAS
    public:
        arbinbus(){ //Constructor de clase arbinbus
            this-> root = nullptr;}
        arbinbus(nodebt<T> *node){
            this-> root = node;}
        virtual ~arbinbus(){} //Destructor de clase arbinbus
        void add(T elem); //Procedimiento para añadir un nodo al arbol que contenga al elemento 'elem'
        bool inArbin(T elem, unsigned int & cost); //Función que devuelve si el elemento esta o no en el arbol
        bool arbinEmpty() const; //Función que devuelve si el arbol está o no vacío
        void deleteArbin();
        T showRoot();
        arbinbus<T> left();
        arbinbus<T> right();


    //ENTIDADES PRIVADAS
    private:
        nodebt<T>* root; //Puntero a la raiz del arbol
        nodebt<T>** arbinSearch(T element, unsigned int & cost); //Función que devuelve un doble puntero al nodo con el elemento deseado
        void arbinSearch(T element, nodebt<T>*** aux, unsigned int & cost); //Procedimiento auxiliar de la primera arbinSearch; devuelve como segundo parámetro un triple puntero al nodo con el elemento deseado
        void showAll(nodebt<T>* node);}; //Procedimiento auxiliar de la función pública showAll; muestra al nodo pasado por parámetro y a todos sus hijos inorder

//IMPLEMENTACIONES

//FUNCIONES Y PROCEDIMIENTOS PÚBLICOS
template <typename T> void arbinbus<T>::add(T elem){
    unsigned int cost;
    nodebt<T>** aux = arbinSearch(elem,cost);
    if (**&aux == nullptr){ //Si el elemento ya no existe
        nodebt<T>* aux1 = new nodebt<T>(elem);
        (**&aux) = &*aux1;;}} //Vincula un nuevo nodo con valor 'elem' en la posición correspondiente

template <typename T> bool arbinbus<T>::inArbin(T elem, unsigned int & cost){
    return ((&**(arbinSearch(elem,cost))) != nullptr);}

template <typename T> bool arbinbus<T>::arbinEmpty() const{
    return (this->tlong == 0);}

template <typename T> void arbinbus<T>::deleteArbin(){
    deleteArbin(root);}

template <typename T> T arbinbus<T>::showRoot(){
    if(root != nullptr)
        return root->elem;
    else
        return "";}

template <typename T> arbinbus<T> arbinbus<T>::left(){
    if(root != nullptr){
        arbinbus* aux = new arbinbus(root->l);
        return *aux;}
    else
        return root;}

template <typename T> arbinbus<T> arbinbus<T>::right(){
    if(root != nullptr){
        arbinbus *aux = new arbinbus(root->r);
        return *aux;}
    else
        return root;}

//FUNCIONES Y PROCEDIMIENTOS PRIVADOS
template <typename T> nodebt<T>** arbinbus<T>::arbinSearch(T element, unsigned int & cost){
    nodebt<T>** aux;
    aux = &this->root;
    cost = 0;
    arbinSearch(element,&aux,cost); //Crea un doble puntero a un nodo, le asigna a ese doble puntero el valor de la memoria de la raiz y llama al otro procedimiento privado arbinSearch con el elemento a buscar y el puntero
    return aux;}

template <typename T> void arbinbus<T>::arbinSearch(T element, nodebt<T>*** aux, unsigned int & cost){
    if (((**aux) != nullptr)&&(((**aux)->elem) != element)){ //Si no lo encontró y si no terminó
        cost++;
        if ((**aux)->elem < element){  //Si el nodo es menor
            *aux = &((**aux)->r);
            arbinSearch(element,aux,cost);} //Buscar por derecha
        else{ //Si el nodo es mayor
            *aux = &((**aux)->l);
            arbinSearch(element,aux,cost);}}} //Buscar por izquierda

#endif // ARBINBUS_H
