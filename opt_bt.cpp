#include "opt_bt.h"
#include "ui_opt_bt.h"
#include <QFileDialog>
#include <QFile>
#include <QMessageBox>
#include <sstream>
#include <graphwidget.h>

opt_bt::opt_bt(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::opt_bt)
{
    ui->setupUi(this);
    ui->table->setRowCount(2);
    ui->table->setColumnWidth(0,200);
    ui->table->setColumnWidth(1,50);
    ui->table->setColumnWidth(2,50);
    ui->sb_cantPalabras->setValue(1);}

opt_bt::~opt_bt(){
    delete ui;}

void opt_bt::on_sb_cantPalabras_valueChanged(int arg1){
    ui->table->setRowCount(arg1+1);}

void opt_bt::on_table_cellChanged(int row, int column){
    int err = 0;
    if(column != 0){
    double aux = 0;
    bool ok = true;
    short int i = 0;
    while((i<ui->table->rowCount()) && (ok == true)){
        aux=aux+ui->table->model()->index(i,column).data().toDouble(&ok);
        if(ui->table->model()->index(i,column).data().toString() == "")
            ok = true;
        i++;}
    if(ok == true){
        if(column == 1)
            ui->l_prob->setText(QString::number(aux));
        else
            ui->l_prob2->setText(QString::number(aux));
        ui->l_probTotal->setText(QString::number(ui->l_prob->text().toDouble() + ui->l_prob2->text().toDouble()));
        if((ui->l_probTotal->text().toDouble() == 1) && (ui->l_prob->text().toStdString() != "ERR") && (ui->l_prob2->text().toStdString() != "ERR"))
            ui->b_calcular->setEnabled(true);
        else
            ui->b_calcular->setDisabled(true);}
    else{
        err = 1;}}
    else
        if(row == 0)
            if(ui->table->model()->index(0,0).data().toString() != "")
                err = 2;
    if(err != 0){
        ui->b_calcular->setDisabled(true);
        if(column == 1)
            ui->l_prob->setText("ERR");
        else if (column == 2)
            ui->l_prob2->setText("ERR");
        ui->l_probTotal->setText("ERR");
        switch(err){
            case 1:{
                QMessageBox msgBox;
                msgBox.setWindowTitle("ERROR");
                msgBox.setText("Las columnas de probabilidades tienen que tener un dato numérico");
                msgBox.setStandardButtons(QMessageBox::Ok);
                msgBox.exec();
                break;}
            case 2:{
                QMessageBox msgBox1;
                msgBox1.setWindowTitle("ERROR");
                msgBox1.setText("La celda (0,0) tiene que estar vacía");
                msgBox1.setStandardButtons(QMessageBox::Ok);
                msgBox1.exec();
                break;}}}}

opt_bt::node** opt_bt::inicMat(int i){
    node** mat = new node*[i];
    for (int j = 0; j < i; j++)
        mat[j] = new node[i];
    return mat;}

double opt_bt::sum(short int i,short int j,short int a){
    double acum = 0;
    for (short int k=i;k <= j;k++) //Recorre el arreglo desde i hasta j
        acum = acum + ui->table->model()->index(k,a).data().toDouble(); //Acumula los valores de las probabilidades
    return acum;} //Devuelve la sumatoria

double opt_bt::min(short int i, short int j, short int& k, node** mat){
    double min=mat[i+1][j].val, aux; //Caso k=i (se asume que el primero es el menor)
    short int aux1=i;
    for (k=i+1;k<j;k++){ //Caso (k!=i) ^ (k!=j); testea cada k y compara si es menor
        aux = mat[i][k-1].val + mat[k+1][j].val;
        if (aux < min){
            min = aux;
            aux1 = k;}}
    aux = mat[i][k-1].val; //Caso k=j
    if (aux<min){
        min = aux;
        aux1 = j;}
    k = aux1; //Devuelve el mínimo
    return min;}

void opt_bt::fillMat(node** mat){
    short int k;
    for (short int j=0;j<((ui->table->rowCount())-1);j++){ //Recorre el arreglo hasta llegar al discernible
        for (short int i=j;i >= 0; i--){
            if (i==j){ //Caso i = j; pone directa la probabilidad del arreglo
                mat[i][j].val = ui->table->model()->index(i+1,1).data().toDouble();
                mat[i][j].pos = i;}
            else{ //Caso i != j; pone la suma de el arreglo entre i y j, más la combinación más óptima válida de celdas
                mat[i][j].val = sum(i+1,j+1,1) + sum(i,j+1,2) + min(i,j,k,mat);
                mat[i][j].pos = k;}}}}

void opt_bt::fillArbinMat(arbinbus<QString>& arbin,node** mat, int i, int j){
    if (i<=j){
        arbin.add(ui->table->model()->index((mat[i][j].pos)+1,0).data().toString());
        fillArbinMat(arbin,mat,i,((mat[i][j].pos)-1)); //Va recursivamente por izquierda
        fillArbinMat(arbin,mat,(mat[i][j].pos)+1,j);}} //Va recursivamente por derecha

void opt_bt::dibujarArbin(arbinbus<QString> arbin,Node *node,double i){
    if(arbin.showRoot() != ""){
        if(arbin.left().showRoot() != ""){
            Node *nodel = new Node(ui->prueba,arbin.left().showRoot(),false);
            nodel->setPos(node->pos().x()-(120*i),node->pos().y()+30);
            ui->prueba->addNode(nodel);
            ui->prueba->addEdge(node,nodel);
            dibujarArbin(arbin.left(),nodel,i+0.5);}
        if(arbin.right().showRoot() != ""){
            Node *noder = new Node(ui->prueba,arbin.right().showRoot(),false);
            noder->setPos(node->pos().x()+(120*i),node->pos().y()+30);
            ui->prueba->addNode(noder);
            ui->prueba->addEdge(node,noder);
            dibujarArbin(arbin.right(),noder,i+0.5);}}}

void opt_bt::on_b_calcular_clicked(){
    int err = 0;
    if((ui->table->model()->index(0,0).data().toString() != "") || (ui->table->model()->index(0,1).data().toString() != "")){
        err = 1;} //(0,0) o (0,1) distintos de vacío
    else{
        bool ok = true;
        if(ui->table->model()->index(0,2).data().toString() == "")
            ok = false;
        short int i = 1;
        while(i<(ui->table->rowCount()-1) && ok){
            if((ui->table->model()->index(i,0).data().toString() == "") || (ui->table->model()->index(i,1).data().toString() == "") || (ui->table->model()->index(i,2).data().toString() == ""))
                ok = false;
            i++;}
        if(!ok)
            err = 2; //Celdas vacías
        else{
            i = 1;
            short int j;
            while(i<(ui->table->rowCount()-1) && ok){
                j = i+1;
                while(j<(ui->table->rowCount()-1) && ok){
                    if(ui->table->model()->index(i,0).data().toString() == ui->table->model()->index(j,0).data().toString())
                        ok = false;
                    j++;}
                i++;}
            if(!ok)
                err = 3;}} //Nodos distintos

    if(err == 0){
        node** mat = inicMat((ui->table->rowCount())-1);
        arbinbus<QString> arbin;
        fillMat(mat);
        fillArbinMat(arbin,mat,0,((ui->table->rowCount())-2));
        ui->l_optimo->setText("Costo óptimo: " + QString::number(mat[0][(ui->table->rowCount())-2].val));
        ui->l_optimo->setAlignment(Qt::AlignCenter);
        if(arbin.showRoot() != ""){
            Node *node = new Node(ui->prueba,arbin.showRoot(),false);
            node->setPos(0,-250);
            ui->prueba->deleteItems();
            ui->prueba->addNode(node);
            dibujarArbin(arbin,node,1);}}
    else{
        switch(err){
            case 1:{
                QMessageBox msgBox;
                msgBox.setWindowTitle("ERROR");
                msgBox.setText("Las celdas (0,0) y (0,1) tienen que estar vacías");
                msgBox.setStandardButtons(QMessageBox::Ok);
                msgBox.exec();
                break;}
            case 2:{
                QMessageBox msgBox1;
                msgBox1.setWindowTitle("ERROR");
                msgBox1.setText("Las celdas de datos no pueden estar vacías");
                msgBox1.setStandardButtons(QMessageBox::Ok);
                msgBox1.exec();
                break;}
            case 3:{
                QMessageBox msgBox1;
                msgBox1.setWindowTitle("ERROR");
                msgBox1.setText("Las palabras no pueden repetirse");
                msgBox1.setStandardButtons(QMessageBox::Ok);
                msgBox1.exec();
                break;}}}}

void opt_bt::on_b_archivo_clicked(){
    QFile file(QFileDialog::getOpenFileName(this, tr("Abrir archivo"), QDir::currentPath(),  tr("Text files (*.txt);;All files (*.*)")));
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        QMessageBox msgBox;
        msgBox.setText("Fallo al abrir archivo");
        msgBox.exec();
    }else{
        ui->table->setRowCount(1);
        ui->table->setItem(0,0,new QTableWidgetItem("",0));
        ui->table->setItem(0,1,new QTableWidgetItem("",0));
        ui->table->setItem(0,2,new QTableWidgetItem(file.readLine(),0));
        QString word;
        short int i = 1;
        while(!file.atEnd()){
            ui->table->setRowCount(i+1);
            word = file.readLine();
            ui->table->setItem(i,0,new QTableWidgetItem(word.section(',',0,0),0));
            ui->table->setItem(i,1,new QTableWidgetItem(word.section(',',1,1),0));
            ui->table->setItem(i,2,new QTableWidgetItem(word.section(',',2,2),0));
            i++;}
        ui->sb_cantPalabras->setValue(i-1);}}
