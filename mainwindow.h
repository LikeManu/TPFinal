#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "opt_bt.h"
#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void on_b_bt_toggled(bool checked);

    void on_b_poli_pressed();

    void on_b_bt_pressed();

private:
    Ui::MainWindow *ui;
    opt_bt *form_bt;
};

#endif // MAINWINDOW_H
